/****************************************************************************************
/**
*	List.h : Header file for List ADT in PA5	
*	Name: Kaile Baran
*	Student ID: kbaran
*	Assignment: Programming Assignment 5
*	Course: CMPS 101
*****************************************************************************************/
#include<string.h>
#include <stdio.h>
#include<stdlib.h>

//#ifndef _LIST_H_INCLUDE_
//#define _LIST_H_INCLUDE_

// Exported Type --------------------------------------------------------------
typedef struct ListObj* List;

// Constructors-Destructors ---------------------------------------------------

// newList()
// Returns reference to new empty List object.
List newList(void);

// freeList()
// Frees all heap memory associated with List *pL, and sets *pL to NULL.
void freeList(List* pL);

// Access functions -----------------------------------------------------------

//length()
//return int of number of Nodes in List
int length(List L);

//index()
//returns int of index value of cursor
int index(List L);

//front()
//data value of front Node of list
// Pre: L != NULL && length() > 0
int front(List L);

//back()
//data value of back Node of list
// Pre: L!= NULL && length() > 0
int back(List L);

//get()
//data value of Node pointed to by cursor
int get(List L);

//equals()
//checks if two Lists are equal irrespective of cursor placement
int equals(List A, List B);

// Manipulation procedures ----------------------------------------------------

//clear()
// Pre: L != NULL
void clear(List L);

//moveFront()
//points cursor to first Node in list
// Pre: L != NULL && length() > 0
void moveFront(List L);

//moveBack()
//points cursor to last Node in list
// Pre: length() > 0
void moveBack(List L);

//movePrev()
//points cursor to Node preceding the current Node
//Pre: L != NULL && length()>0 
void movePrev(List L);

//moveNext()
//points cursor to Node after the current Node
//Pre: L != NULL 
void moveNext(List L);

//prepend()
//adds new Node containing value of data to front of L
void prepend(List L, int data);

//append()
//adds new Node containing value of data to back of L
void append(List L, int data);

//insertBefore()
//adds Node to index before current cursor index
void insertBefore(List L, int data);

//insertAfter()
//adds Node to index after current cursor index
void insertAfter(List L, int data);

//deleteFront()
//frees Node at front of list
//Pre: length()>0
void deleteFront(List L);

//deleteBack()
//frees Node at back of list
//Pre: length>0
void deleteBack(List L);

//delete()
//deleted Node currently pointed to by cursor
void delete(List L);

// Other operations -----------------------------------------------------------

//printList()
//prints L to file pointed to by out
void printList(FILE* out, List L);

//copyList()
//returns a new List identical in contents to L
List copyList(List L);