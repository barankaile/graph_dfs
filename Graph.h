/****************************************************************************************
/**
*	Graph.h : Header file for Graph ADT in PA5
*	Name: Kaile Baran
*	Student ID: kbaran
*	Assignment: Programming Assignment 5
*	Course: CMPS 101
*****************************************************************************************/


#ifndef _GRAPH_H_INCLUDE_

#include"List.h"

#define _GRAPH_H_INCLUDE_

#define UNDEF -1

#define NIL 0


// Exported type --------------------------------------------------------------

typedef struct GraphObj* Graph;

// Constructors-Destructors ---------------------------------------------------

Graph newGraph(int n);

void freeGraph(Graph* pG);

// Access functions -----------------------------------------------------------

int getOrder(Graph G);

int getSize(Graph G);

int getParent(Graph G, int u);

int getDiscover(Graph G, int u);

int getFinish(Graph G, int u);

// Manipulation procedures ----------------------------------------------------

void addEdge(Graph G, int u, int v);

void addArc(Graph G, int u, int v);

void DFS(Graph G, List s);

// Other Functions ------------------------------------------------------------

Graph transpose(Graph G);

Graph copyGraph(Graph G);

void printGraph(FILE* out, Graph G);

#endif